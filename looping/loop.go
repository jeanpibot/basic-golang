package main

import "fmt"

func main() {
	// for i := 0; i < 10; i = i + 2 {
	// 	fmt.Println(i)
	// 	if i%2 == 0 {
	// 		i /= 2
	// 	} else {
	// 		i = 2*i + 1
	// 	}
	// }

	// for i := 0; i < 10; i++ {
	// 	fmt.Println(i)
	// 	if i%2 == 0 {
	// 		i /= 2
	// 	} else {
	// 		i = 2*i + 1
	// 	}
	// }

	// esta es una etiqueta que nos permite cerrar todo el loop desde donde se pone
	// loop:
	// 	for i := 1; i <= 5; i++ {
	// 		for j := 1; j <= 3; j++ {
	// 			fmt.Println(i * j)
	// 			if i*j >= 3 {
	// 				break loop
	// 			}
	// 		}
	// 	}

	// loop con más sentencias
	// for i, j := 0, 0; i < 5; i, j = i+1, j+2 {
	// 	fmt.Println(i, j)
	// }

	// collections con loop
	statePopulations := make(map[string]int, 10)
	// Aqui vamos a prender que es maps
	statePopulations = map[string]int{
		"Bogota":    7413000000,
		"Medellin":  2427000000,
		"Cali":      2228000000,
		"Neiva":     357392,
		"Florencia": 168346,
	}
	for keys, values := range statePopulations {
		fmt.Println(keys, values)
	}

	// la forma para leer solo la lleve sin leer el valor se realiza de la siguiente manera
	for k := range statePopulations {
		fmt.Println(k)
	}

	//ahora la forma de leer solo los valores sin la llave se realiza de la siguiente manera
	for _, v := range statePopulations {
		fmt.Println(v)
	}

	// Para recorrer el string
	// s := "Hello JP"
	// for j, z := range s {
	// 	fmt.Println(j, string(z))
	// }
}
