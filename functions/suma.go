package main

import "fmt"

//Suma es una funcion que suma los valores
func Suma(values ...int) (result int) {
	fmt.Println(values)
	for _, v := range values {
		result += v
	}
	return
}

//Divide es una funcion que divide valores
func Divide(a, b float64) (float64, error) {
	if b == 0.0 {
		return 0.0, fmt.Errorf("Can not divide by zero")
	}
	return a / b, nil
}
